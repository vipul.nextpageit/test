const fields = [
  {
    fieldId: "5",
    fieldName: "city",
    fieldLabel: "City",
    required: true,
    type: "text",
    informationLabel: "Create a brand for ",
    sectionName: "Address Information",
    order: 5,
    validationType: "string",
    validations: [{
      type: "required",
      params: ["City is required"]
    }]
  },
  {
    fieldId: "2",
    fieldName: "companyCode",
    fieldLabel: "Company Code",
    required: true,
    type: "text",
    informationLabel: "Create a brand for ",
    sectionName: "Brand Information",
    order: 2,
    validationType: "string",
    validations: [{
      type: "required",
      params: ["Company code is required"]
    }]
  },
  {
    fieldId: "3",
    fieldName: "currency",
    fieldLabel: "Currency",
    required: false,
    type: "dropDown",
    option: [
      { optionName: "usd", optionLabel: "USD", order: 1 },
      { optionName: "inr", optionLabel: "INR", order: 2 },
      { optionName: "aud", optionLabel: "AUD", order: 3 }
    ],
    informationLabel: "Create a brand for ",
    sectionName: "Brand Information",
    validationType: "string",
    validations: [{
      type: "required",
      params: ["Currency is required"]
    }],
    order: 3
  },
  {
    fieldId: "1",
    fieldName: "companyName",
    fieldLabel: "Company Name",
    required: true,
    type: "text",
    informationLabel: "Create a brand for ",
    sectionName: "Brand Information",
    order: 1,
    validationType: "string",
    validations: [{
      type: "required",
      params: ["Company name is required"]
    }]
  },
  {
    fieldId: "8",
    fieldName: "additionalInformation",
    fieldLabel: "Additional Information",
    required: false,
    type: "multiline",
    informationLabel: "Create a brand for ",
    sectionName: "Additional Information",
    order: 8
  },
  {
    fieldId: "7",
    fieldName: "country",
    fieldLabel: "Country",
    required: true,
    type: "text",
    informationLabel: "Create a brand for ",
    sectionName: "Address Information",
    order: 7
  },
  {
    fieldId: "4",
    fieldName: "portalLanguage",
    fieldLabel: "Portal Language",
    required: true,
    type: "radio",
    option: [{ optionName: "English", optionLabel: "USD", order: 1 }],
    informationLabel: "Create a brand for ",
    sectionName: "Brand Information",
    order: 4
  },
  {
    fieldId: "6",
    fieldName: "state",
    fieldLabel: "State",
    required: true,
    type: "text",
    informationLabel: "Create a brand for ",
    sectionName: "Address Information",
    order: 6
  }
];
export const inputFields= fields