import React, {useState} from "react";
import {inputFields} from '../data'
import { MDBContainer, MDBRow, MDBCol,MDBBtn} from 'mdbreact';
import {useFormik} from 'formik'

import MainInput from "./MainInput";
import {getYupSchemaFromMetaData} from "../yupValidationSchema";
//test schema

import Welcome from "../components/Welcome";
// const testSchema = Yup.object().shape({
//     city: Yup.string()
//         .min(2, 'Too Short!')
//         .max(50, 'Too Long!')
//         .required('Required'),
//     companyCode: Yup.string()
//         .min(2, 'Too Short!')
//         .max(50, 'Too Long!')
//         .required('Required'),
//     email: Yup.string().email('Invalid email').required('Required'),
// });

const FormComp = ()=>{
    const [formData,setFormdata] = useState(null)
    const validateSchema = getYupSchemaFromMetaData(inputFields, [], []);
    const formik = useFormik({
        initialValues: {
        },
        onSubmit: values => {

            setFormdata(values)
        },
        validationSchema:validateSchema
    });
    console.log(formik)

return(
    <div  className="main">

        {formData == null?<MDBContainer>
            <MDBRow className="central-content">
                <MDBCol md="6">
                    <form onSubmit={formik.handleSubmit}>
                        <p className="h5 mb-1">Enter Details</p>
                        <div className="grey-text">
                            <MainInput config={inputFields} formik={formik}/>


                        </div>
                        <MDBBtn type={"submit"} color="primary">Submit</MDBBtn>
                    </form>
                </MDBCol>

            </MDBRow>

        </MDBContainer>:
<Welcome formData={formData}/>
        }

    </div>
)
}
export default FormComp