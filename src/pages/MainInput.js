import React from 'react';
import {MDBInput} from "mdbreact";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import {FormControlLabel, FormLabel, Radio, RadioGroup} from "@material-ui/core";


const MainInput = ({config, formik}) => {
    const builder = (individualConfig) => {
        switch (individualConfig.type) {
            case 'text':
                return (
                    <>

                        <MDBInput className="main-input" label={individualConfig.fieldLabel}  onChange={formik.handleChange}  group type="text"  id={individualConfig.fieldId} name={individualConfig.fieldName.split(' ').join('')}  />
                        {Object.keys(formik.errors).includes(individualConfig.fieldName)?<span className="font-small text-danger">{formik.errors[individualConfig.fieldName]}</span>:""}
                        </>
                );
            case 'multiline':
                return (
                    <>
                        <div className="input-group">
                            <MDBInput
                                className="main-input"
                                id={individualConfig.fieldId}
                                name={individualConfig.fieldName.split(' ').join('')}
                                type="textarea"
                                label={individualConfig.fieldLabel}
                                rows="2"

                                onChange={formik.handleChange}
                            />

                        </div>
                    </>
                )
            case 'dropDown':
                return (
                    <FormControl className={"form-input"}>
                        <InputLabel id="demo-mutiple-name-label">{individualConfig.fieldLabel}</InputLabel>
                        <Select
                            labelId="demo-mutiple-name-label"
                            id="demo-mutiple-name"
name={individualConfig.fieldName.split(' ').join('')}

                            onChange={formik.handleChange}
                            input={<Input />}

                        >
                            {individualConfig.option?
                                individualConfig.option.map((option,index)=>{
                                    return(

                                        <MenuItem key={index} value={option.optionLabel} name={option.optionName}>{option.optionLabel}</MenuItem>
                                    )
                                }):""
                            }

                        </Select>
                        {Object.keys(formik.errors).includes(individualConfig.fieldName)?<span className="font-small text-danger">{formik.errors[individualConfig.fieldName]}</span>:""}
                    </FormControl>
                )
            case 'radio':
                return (
                    <FormControl component="fieldset">
                        <FormLabel component="legend">{individualConfig.fieldLabel}</FormLabel>
                        <RadioGroup aria-label={individualConfig.informationLabel} name={individualConfig.fieldName}  onChange={formik.handleChange}>
                            {individualConfig.option?
                                individualConfig.option.map((option,index)=>{
                                    return(

                                        <FormControlLabel defaultChecked={false} control={<Radio />} label={option.optionLabel} />
                                    )
                                }):""
                            }

                        </RadioGroup>
                    </FormControl>
                )
            case 'array':
                return (
                    <MainInput config={individualConfig.children || []} formik={formik} />
                );
            default:
                return <div>Unsupported field</div>
        }
    }

    return (
        <>
            {config.map((c) => {
                return builder(c);
            })}
        </>
    );
};

export default MainInput;