import React from "react";
const Welcome = ({formData})=>{
    return(
        <div className="bounceIn">
            <h4>Welcome new user from {formData.city?formData.city:""}</h4>
        </div>
    )
}
export default Welcome;