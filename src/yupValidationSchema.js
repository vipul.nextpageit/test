import * as yup from 'yup';






const validator = function (message) {

    return this.test('is-string-boolean', message, function (value) {
        if (isEmpty(value)) {
            return true;
        }

        if (['Y', 'N'].indexOf(value) !== -1) {
            return true;
        } else {
            return false;
        }
    });
};

yup.addMethod(yup.string, "stringBoolean", validator);
yup.addMethod(yup.string, "StringBoolean", validator);
const isEmpty = (value)=>{
    return value.length>0
}



export function createYupSchema(schema, config) {
    const { fieldName, validationType, validations = [] } = config;

    if (!yup[validationType]) {
        return schema;
    }
    let validator = yup[validationType]();
    validations.forEach((validation) => {
        const { params, type } = validation;
        if (!validator[type]) {
            return;
        }
        validator = validator[type](...params);

    });
    if (fieldName.split(" ").join("").indexOf('.') !== -1) {

    } else {
        schema[fieldName.split(" ").join("")] = validator;
    }
    console.log('Schema',validator)
    return schema;
}

export const getYupSchemaFromMetaData = (
    metadata,
    additionalValidations,
    forceRemove
) => {

    const yepSchema = metadata.reduce(createYupSchema, {});
    const mergedSchema = {
        ...yepSchema,
        ...additionalValidations,
    };

    forceRemove.forEach((field) => {
        delete mergedSchema[field];
    });

    const validateSchema = yup.object().shape(mergedSchema);

    return validateSchema;
};